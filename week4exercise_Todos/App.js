import React, { Component } from 'react'
import Container from './src/routes'
import { Provider } from 'react-redux'
import store from './src/api/store'


class App extends Component{
    render() {
        return (
        <Provider store = {store}>
            <Container />
        </Provider>
        )
    }
}

export default App
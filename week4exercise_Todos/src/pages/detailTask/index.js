import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, TextInput, FlatList, ScrollView } from 'react-native'
import DatePicker from 'react-native-datepicker'
import Axios from 'axios'


export default class DetailTask extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props){
        super(props)
        const { itemId, itemTitle, itemBody, itemCreated, itemUpdated, itemDay, itemStatus, itemComment } = this.props.navigation.state.params
        this.state = {
            task : {
            _id : itemId,
            title: itemTitle,
            body : itemBody,
            created : itemCreated,
            updated : itemUpdated,
            day : itemDay,
            status : itemStatus,
            comment : itemComment,
            },
            isloading: false
    
        }
        
    }

    render() {
        return (
            <ScrollView>
            <View style={{ backgroundColor: 'lightpink', flex: 1 }}>
                <View style={{ marginHorizontal: 20, marginVertical: 20, flex: 1 }}>
                <View style={styles.container}>
                    <View style={{ marginLeft: 20 }}>
                    <TextInput
                                style={ styles.text }
                                value={ this.state.task.title }
                                keyboardType='default'
                                onChangeText={text => this.setState({
                                    task: {...this.state.task, title: text }} )} />
                        <View style={styles.bar} />
                        <TextInput
                                style={ styles.text }
                                value={ this.state.task.body }
                                keyboardType='default'
                                onChangeText={text => this.setState({
                                    task: {...this.state.task, body: text }} )} />
                        <View style={styles.bar} />
                        <DatePicker
                            style={{ width: 110 }}
                            date={this.state.task.created}
                            mode="date"
                            placeholder="select date"
                            format="YYYY-MM-DD"
                            minDate={new Date}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    marginLeft: 30
                                }
                            }}
                            onDateChange={(created) => {this.setState({task: {...this.state.task, created: created}})}}
                        />
                        <View style={styles.bar} />
                    </View>
                    <View style={{ marginRight: 20 }}>
                        <TextInput
                                style={ styles.text }
                                value={ this.state.task.status }
                                keyboardType='default'
                                onChangeText={text => this.setState({
                                    task: {...this.state.task, status: text }} )} />
                        <View style={styles.bar} />
                        <Text style={styles.text}>{this.state.task.day}</Text>
                        <View style={styles.bar} />
                    </View>
                </View>
                <View style={styles.vaccinationWrap}>
                    <Text style={styles.vaccinationText}>{this.state.task.comment}</Text>
                </View>
            </View>
            </View>
        </ScrollView>
        )
    }

    async componentWillUnmount() {
        console.log(this.state.task)
    await Axios.put(`http://192.168.56.1:3000/api/v1/todos/${this.state.task.created}`,this.state.task)
    }
}


    const styles = StyleSheet.create({
        mainwrapper: {
            backgroundColor: 'lightpink', flex: 1
        },
        container: {
            marginHorizontal: 10, padding: 15, borderRadius: 40, backgroundColor: '#73C6B6', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', opacity: 0.8
        },
        text: {
            fontSize: 15, fontWeight: 'bold'
        },
        bar: {
            backgroundColor: 'white', width: '100%', height: 2, marginVertical: 5
        },
        img: {
            borderColor: '#73C6B6', borderWidth: 10, width: '100%', height: 200, borderRadius: 100, paddingTop: 5,
            backgroundColor: "#000000",
            opacity: 0.75,
            alignItems: "center"
        },
        overlay: {
            paddingTop: 5,
            backgroundColor: "#000000",
            opacity: 0.5,
            flexDirection: "column",
            alignItems: "center",
        },
        vaccinationWrap: {
            opacity: 0.8, marginTop: 12, marginHorizontal: 12, backgroundColor: '#73C6B6', borderRadius: 30, padding: 10
        },
        vaccinationText: {
            textAlign: "center", fontSize: 16, fontWeight: 'bold'
        }
    })

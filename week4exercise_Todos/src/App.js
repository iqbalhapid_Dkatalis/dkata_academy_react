import React, { Component } from 'react'
import { View, ScrollView, StyleSheet, TouchableOpacity, Text, FlatList } from 'react-native'
import Heading from './components/heading/heading'
import Input from './components/input/input'
import Button from './components/button/Button';
import TodoList from './components/list/TodoList';
import TabBar from './components/tabBar/TabBar';
import Myheader from './components/heading/MyHeader';
import renderItem from './components/renderItem'

import { getTask } from './api/action'
import { connect } from 'react-redux'

let todoIndex = 0;

class App extends Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props)
        this.state = {
            inputValue: '', todos: [],
            type: 'All'
            }
        }

        componentDidMount() {
            this.props.getTask()
        }

        setType = (type) => {
            this.setState({ type })
            }

        inputChange = (inputValue) =>{
            console.log(inputValue)
            this.setState({inputValue})
        }

        submitTodo = () =>{
            if(this.state.inputValue.match(/^\s*$/)) return
            let todo = {
                title: this.state.inputValue,
                todoIndex: todoIndex,
                complete: false
            }
            todoIndex++
            this.state.todos.push(todo)
            //const todos = [...this.state.todos, todo]
            this.setState({todos: this.state.todos, inputValue: ''}, () => {
                console.log('State: ',this.state)
            })
        }

        deleteTodo = (todoIndex) => {
            let { todos } = this.state
            todos = todos.filter((todo) => todo.todoIndex !== todoIndex)
            this.setState({ todos })
        }

        toggleComplete = (todoIndex) => {
                let todos = this.state.todos
                todos.forEach((todo) => {
                if (todo.todoIndex === todoIndex) {
                todo.complete = !todo.complete
                }
            })
        this.setState({ todos })
        }

    render() {
        const { todos, inputValue, type } = this.state
        return (
            // <View style={{ backgroundColor: 'lightpink', flex: 1, paddingTop: 10 }}>
            //     {/* <NavigationEvents onDidFocus = {() => this.getPets()} /> */}
            //     <Myheader />
                
            // </View>
            <View style={styles.container}>
                <Myheader />
                <Heading />
                <Input
                    inputValue={inputValue}
                    inputChange={(text) => this.inputChange(text)} />
                    <TodoList type={type} toggleComplete={this.toggleComplete} deleteTodo={this.deleteTodo} todos={todos} />
                {/* <Text>see details</Text> */}
                <Button submitTodo={this.submitTodo} />
                <ScrollView keyboardShouldPersistTaps='always' style={styles.content}>
                    <FlatList
                        data={this.props.task.task}
                        keyExtractor={({ _id }, index) => _id}
                        renderItem={(item) => renderItem(item, this.props.navigation)}
                    />
                </ScrollView>
                <TabBar type={type} setType={this.setType} />
            </View>
        )
    }
    
}
const mapStateToProps = state => ({
    task: state.task
})


export default connect(mapStateToProps, {getTask})(App);

const styles = StyleSheet.create({
container: {
    flex: 1,
    backgroundColor: '#f5f5f5'
},
content: {
    flex: 1
}
});


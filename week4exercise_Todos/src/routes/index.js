import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import App from '../App';
import detailTodos from '../pages/detailTask'

const RootStack = createStackNavigator(
    {

        Home: { screen: App },
        Detail: { screen: detailTodos },
    },

    {

        initialRouteName: 'Home',
    }

);

const Container = createAppContainer(RootStack);

export default Container;
import React, {Component} from 'react'
import {Text, View, StatusBar} from 'react-native'

export default class Header extends Component {
    render(){
        return(
            <View style = {{backgroundColor: 'lightpink', padding: 5}}>
                <StatusBar backgroundColor='lightpink'/>
                <Text style = {{fontSize: 40, color: 'white', textAlign: "center", fontWeight: 'bold', marginVertical: 10}}>TODO's</Text>
            </View>
        )
    }
}
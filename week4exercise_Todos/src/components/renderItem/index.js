import React from 'react'
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native'

export default renderItem = ({item}, navigation) => {
    return(
    <TouchableOpacity onPress={ () => navigation.navigate('Detail', { itemId : item._id, itemTitle : item.title, 
    itemBody: item.body, itemCreated : item.created_for,  itemUpdated : item.updated_at, itemDay: item.day, itemComments: item.comments , itemStatus: item.status })}>
    <View style={styles.wrapper}>
        <View style={styles.bar}></View>
        <View style={styles.container}>
            <View>
                <Text style={styles.textHeader}>Title :</Text>
                <Text style={styles.text}>{item.title}</Text>
                <View style={styles.bar}></View>
                <Text style={styles.textHeader}>Description :</Text>
                <Text style={styles.text}>{item.body}</Text>
                <View style={styles.bar}></View>
                <Text style={styles.textHeader}>Created For :</Text>
                <Text style={styles.text}>{item.created_for.substring(0,10)}</Text>
                <View style={styles.bar}></View>
            </View>
            <View>
                <Text style={styles.textHeader}>Day :</Text>
                <Text style={styles.text}>{item.day}</Text>
                <View style={styles.bar}></View>
                <Text style={styles.textHeader}>Updated :</Text>
                <Text style={styles.text}>{item.updated_at}</Text>
                <View style={styles.bar}></View>
                <Text style={styles.textHeader}>Status :</Text>
                <Text style={styles.text}>{item.status}</Text>
                <View style={styles.bar}></View>

            </View>
            
        </View>
    </View>
    </TouchableOpacity>
)}

const styles = StyleSheet.create({
    wrapper : {
        backgroundColor: '#f5f5f5'
    },
    bar: {
        backgroundColor: '#f5f5f5', width: '100%', height: 3, marginVertical: 5
    },
    container: {
        flexDirection: 'row', alignContent: 'center', justifyContent: 'space-around', backgroundColor: 'lightpink',
        borderRadius: 30, padding: 18, marginHorizontal: 10
    },
    textHeader: {
        fontSize: 10, color: 'gray'
    },
    text: {
        fontSize: 10, fontWeight: 'bold', color: 'white'
    }
})
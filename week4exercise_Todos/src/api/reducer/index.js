import {combineReducers} from 'redux'
import  getTaskReducer  from './getTaskReducer'

const appReducer = combineReducers({
    task: getTaskReducer
})

const rootReducer = (state, action) => {
    return appReducer(state, action)
}

export default rootReducer


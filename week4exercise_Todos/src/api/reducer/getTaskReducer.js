const initialState = {
    task: [],
    loading: true,
    error: null
}


export const GET_API = 'GET_API'
export const GET_API_SUCCESS = 'GET_API_SUCCESS'
export const GET_API_FAIL = 'GET_API_FAIL'

function reducer(state = initialState, action) {
    console.log(action)
    switch(action.type){
        case GET_API :
            return {...state, loading: action.payload}
        case GET_API_SUCCESS :
            return {...state, task: action.payload, loading: action.loading}
        case GET_API_FAIL :
            return {
                ...state,
                loading : action.loading,
                error : action.error
            }
        default :
        return state;
    }
}

export default reducer;

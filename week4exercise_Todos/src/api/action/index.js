import Axios from 'axios'
import { GET_API, GET_API_SUCCESS, GET_API_FAIL} from '../reducer/getTaskReducer'

const fetchData = (bool) => {
    return {
        type :  GET_API,
        payload : bool
    }
}

const fetchDataSuccess = (data) => {
    return {
        type: GET_API_SUCCESS,
        payload : data,
        loading: false
    }
}

const fetchDataRejected = (error) => {
    return {
        type: GET_API_FAIL,
        payload: error,
        loading: false
    }
}

export const getTask = () => {
    return dispatch => {
        dispatch(fetchData(true))
            const url = 'http://192.168.56.1:3000/api/v1/todos'
            Axios.get(url).then(results => dispatch(fetchDataSuccess(results.data)))   
        .catch ((error) => {
            dispatch(fetchDataRejected(error))
        })
    }
}

import React, {Component} from 'react'
import {Text, View} from 'react-native'
import BtnAdd from '../ButtonAdd'

export default class Header extends Component {
    render(){
        return(
            <View style = {{backgroundColor: 'lightpink', padding: 5}}>
                <BtnAdd />
                <Text style = {{fontSize: 40, color: 'white', textAlign: "center", fontWeight: 'bold'}}>My Pets</Text>
            </View>
        )
    }
}
import React, { Component } from 'react'
import {View, FlatList, ActivityIndicator, StatusBar } from 'react-native'
import {NavigationEvents} from 'react-navigation'
import renderItem from './renderItem'
import Axios from 'axios'
import Header from '../Header'
import {bindActionCreators} from 'redux'

import { getPets } from '../../actions'
import { connect } from 'react-redux'
class Pets extends Component {
    
    static navigationOptions = {
        header : null
        };

    constructor(props) {
        super(props)
        // this.state = {
        //     pets: [],
        //     isloading : false
        // }
    }

    componentDidMount() {
        this.props.getPets()
    }

    render() {
        if(this.props.pets.loading){
            return(
                <View>
                    <StatusBar backgroundColor={'lightpink'} />
                    <ActivityIndicator color={'white'} size={'large'} />
                </View>
            )
        }
        return (
            <View style={{backgroundColor: 'lightpink', flex: 1, paddingTop: 10 }}>
                {/* <NavigationEvents onDidFocus = {() => this.getPets()} /> */}
                <Header />
                <FlatList
                    data={this.props.pets.pets}
                    keyExtractor={({ id }, index) => id}
                    renderItem={(item) =>renderItem(item, this.props.navigation)}
                />
            </View>
        );
    }

    
}
const mapStateToProps = state => ({
    pets: state.pets
})


export default connect(mapStateToProps, {getPets})(Pets);
import React from 'react'
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native'

export default renderItem = ({item}, navigation) => {
    return(
    <TouchableOpacity onPress={ () => navigation.navigate('Profile', { itemId : item.id, itemName : item.name, 
    itemBreed: item.breed, itemNextCheck : item.next_checkup, itemAge: item.age, itemColour: item.colour , itemVaccination: item.vaccinations, itemPath: item.path })}>
    <View style={styles.wrapper}>
        <View style={styles.bar}></View>
        <View style={styles.container}>
            <View>
                <Text style={styles.textHeader}>Name :</Text>
                <Text style={styles.text}>{item.name}</Text>
                <View style={styles.bar}></View>
                <Text style={styles.textHeader}>Breed :</Text>
                <Text style={styles.text}>{item.breed}</Text>
                <View style={styles.bar}></View>
                <Text style={styles.textHeader}>next_checkup :</Text>
                <Text style={styles.text}>{item.next_checkup.substring(0,10)}</Text>
                <View style={styles.bar}></View>
            </View>
            <View>
                <Text style={styles.textHeader}>Age :</Text>
                <Text style={styles.text}>{item.age}</Text>
                <View style={styles.bar}></View>
                <Text style={styles.textHeader}>Colour :</Text>
                <Text style={styles.text}>{item.colour}</Text>
                <View style={styles.bar}></View>
            </View>
            
        </View>
    </View>
    </TouchableOpacity>
)}

const styles = StyleSheet.create({
    wrapper : {
        backgroundColor: 'lightpink'
    },
    bar: {
        backgroundColor: 'pink', width: '100%', height: 2, marginVertical: 5
    },
    container: {
        flexDirection: 'row', alignContent: 'center', justifyContent: 'space-around', backgroundColor: 'white',
        borderRadius: 30, padding: 18, marginHorizontal: 10
    },
    textHeader: {
        fontSize: 16, color: 'pink'
    },
    text: {
        fontSize: 16, fontWeight: 'bold', color: 'gray'
    }
})
import React, { Component } from 'react'
import { Text, View, TextInput, TouchableOpacity } from 'react-native'
import Axios from 'axios'


export default class PostPet extends Component {
    constructor(props) {
        super(props)

    }

    state = {
        isloading: false,
        pet: {
            id: '',
            date: '',
            name: '',
            breed: '',
            age: '',
            colour: '',
            vaccination: '',
            path: ''
        }
    }

    _handlePostPet = async () => {
        console.log(this.state.pet)
        await Axios.put(`http://192.168.133.2:3000/api/v1/mypets/${this.state.pet.id}`, this.state.pet, {
            headers: { 'Authorization': 'Basic am9objpzZWNyZXQ=' }
        })
    }

    _handleImgPick = () => {
        const options = {
            title: 'Select Avatar',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };


        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                const source = { uri: response.uri };

                this.setState({
                    avatarSource: source,
                });
            }
        });
    }


    // maxId = await result.aggregate([{$group:{_id:"maxId", max:{$max: "$id"}}}])
    // newId = maxId[0].max + 1
    // Object.assign(request.payload, {id : newId, sold : false})
    // newPet = await result.insertMany([request.payload])
    // h.response().code(201)
    // return newPet




    render() {
        return (
            <ScrollView>
                <View style={{ backgroundColor: 'lightpink', flex: 1 }}>
                    <View style={{ marginHorizontal: 20, marginVertical: 20 }}>
                        <View>
                            <View>
                                <Text>this is addPet Page</Text>
                            </View>
                            <View>
                                <Text>
                                    Name :
                                </Text>
                                <TextInput
                                    style={styles.text}
                                    onChangeText={(name) => this.setState({ name })} 
                                />

                                <Text>
                                    Breed :
                                </Text>
                                <TextInput
                                    style={styles.text}
                                    onChangeText={(name) => this.setState({ breed })} 
                                />

                                <DatePicker
                                    style={{ width: 110 }}
                                    date={this.state.pet.date}
                                    mode="date"
                                    placeholder="select date"
                                    format="YYYY-MM-DD"
                                    minDate={new Date}
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    customStyles={{
                                        dateIcon: {
                                            position: 'absolute',
                                            left: 0,
                                            top: 4,
                                            marginLeft: 0
                                        },
                                        dateInput: {
                                            marginLeft: 30
                                        }
                                    }}
                                    onDateChange={(date) => { this.setState({ pet: { ...this.state.pet, date: date } }) }}
                                />
                                
                                <TextInput placeholder=" please type yout pet breed"></TextInput>
                                <TextInput placeholder=" please type yout pet age"></TextInput>
                                <TextInput placeholder=" please type yout pet colour"></TextInput>
                                <TextInput placeholder=" please type yout pet vaccination"></TextInput>
                            </View>
                        </View>
                        <TouchableOpacity onPress={this._handleImgPick}>
                            <Image style={styles.img} source={this.state.avatarSource ? this.state.avatarSource :
                                {
                                    uri: this.state.pet.path, headers: {
                                        Authorization: 'Basic am9objpzZWNyZXQ='
                                    }
                                }
                            } />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.container}>
                        <View style={{ marginLeft: 20 }}>
                            <Text style={styles.text}>{this.state.pet.name}</Text>
                            <View style={styles.bar} />
                            <Text style={styles.text}>{this.state.pet.breed}</Text>
                            <View style={styles.bar} />
                            <DatePicker
                                style={{ width: 110 }}
                                date={this.state.pet.date}
                                mode="date"
                                placeholder="select date"
                                format="YYYY-MM-DD"
                                minDate={new Date}
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateIcon: {
                                        position: 'absolute',
                                        left: 0,
                                        top: 4,
                                        marginLeft: 0
                                    },
                                    dateInput: {
                                        marginLeft: 30
                                    }
                                }}
                                onDateChange={(date) => { this.setState({ pet: { ...this.state.pet, date: date } }) }}
                            />
                            <View style={styles.bar} />
                        </View>
                        <View style={{ marginRight: 20 }}>
                            <TextInput
                                style={styles.text}
                                value={(this.state.pet.age).toString()}
                                keyboardType='number-pad'
                                onChangeText={text => this.setState({
                                    pet: { ...this.state.pet, age: text }
                                })} />
                            <View style={styles.bar} />
                            <Text style={styles.text}>{this.state.pet.colour}</Text>
                            <View style={styles.bar} />
                        </View>
                    </View>
                    <View style={styles.vaccinationWrap}>
                        <Text style={styles.vaccinationText}>{this.state.pet.vaccination[0]}</Text>
                        <Text style={styles.vaccinationText}>{this.state.pet.vaccination[1]}</Text>
                        <Text style={styles.vaccinationText}>{this.state.pet.vaccination[2]}</Text>
                    </View>
                </View>
            </ScrollView>
        )
    }
} 
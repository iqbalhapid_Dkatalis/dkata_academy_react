import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, TextInput, FlatList, ScrollView } from 'react-native'
import DatePicker from 'react-native-datepicker'
import ImagePicker from 'react-native-image-picker'
import Axios from 'axios'


export default class profilePets extends Component {
    static navigationOptions = {
        title: "Pet Profile",
        headerStyle: {
            backgroundColor: "#73C6B6"
        }
    };
    constructor(props){
        super(props)
        this.state = {
            pet : {
            id : this.props.navigation.state.params.itemId,
            date: this.props.navigation.state.params.itemNextCheck,
            name : this.props.navigation.state.params.itemName,
            breed : this.props.navigation.state.params.itemBreed,
            age : this.props.navigation.state.params.itemAge,
            colour : this.props.navigation.state.params.itemColour,
            vaccination : this.props.navigation.state.params.itemVaccination,
            path : this.props.navigation.state.params.itemPath,
            },
            isloading: false,
            avatarSource : '',
    
        }
        
    }

    _handleImgPick = () => {
        const options = {
            title: 'Select Avatar',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
    

    ImagePicker.showImagePicker(options, (response) => {
        if (response.didCancel) {
        } else if (response.error) {
        } else if (response.customButton) {
        } else {
            const source = { uri: response.uri };

            this.setState({
                avatarSource: source,
            });
        }
    });
    }

    render() {
        return (
            <ScrollView>
            <View style={{ backgroundColor: 'lightpink', flex: 1 }}>
                <View style={{ marginHorizontal: 20, marginVertical: 20 }}>
                    <TouchableOpacity onPress={this._handleImgPick}>
                        <Image style={styles.img} source={this.state.avatarSource ? this.state.avatarSource :
                            {
                                uri: this.state.pet.path, headers: {
                                    Authorization: 'Basic am9objpzZWNyZXQ='
                                }
                            }
                        } />
                    </TouchableOpacity>
                </View>
                <View style={styles.container}>
                    <View style={{ marginLeft: 20 }}>
                        <Text style={styles.text}>{this.state.pet.name}</Text>
                        <View style={styles.bar} />
                        <Text style={styles.text}>{this.state.pet.breed}</Text>
                        <View style={styles.bar} />
                        <DatePicker
                            style={{ width: 110 }}
                            date={this.state.pet.date}
                            mode="date"
                            placeholder="select date"
                            format="YYYY-MM-DD"
                            minDate={new Date}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    marginLeft: 30
                                }
                            }}
                            onDateChange={(date) => {this.setState({pet: {...this.state.pet, date: date}})}}
                        />
                        <View style={styles.bar} />
                    </View>
                    <View style={{ marginRight: 20 }}>
                        <TextInput
                                style={ styles.text }
                                value={ (this.state.pet.age).toString() }
                                keyboardType='number-pad'
                                onChangeText={text => this.setState({
                                    pet: {...this.state.pet, age: text }} )} />
                        <View style={styles.bar} />
                        <Text style={styles.text}>{this.state.pet.colour}</Text>
                        <View style={styles.bar} />
                    </View>
                </View>
                <View style={styles.vaccinationWrap}>
                    <Text style={styles.vaccinationText}>{this.state.pet.vaccination[0]}</Text>
                    <Text style={styles.vaccinationText}>{this.state.pet.vaccination[1]}</Text>
                    <Text style={styles.vaccinationText}>{this.state.pet.vaccination[2]}</Text>
                </View>
            </View>
        </ScrollView>
        )
    }

    async componentWillUnmount() {
        console.log(this.state.pet)
    await Axios.put(`http://192.168.133.2:3000/api/v1/mypets/${this.state.pet.id}`,this.state.pet, {
            headers: { 'Authorization': 'Basic am9objpzZWNyZXQ=' }
        })
    }
}


    const styles = StyleSheet.create({
        mainwrapper: {
            backgroundColor: 'lightpink', flex: 1
        },
        container: {
            marginHorizontal: 10, padding: 15, borderRadius: 40, backgroundColor: '#73C6B6', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', opacity: 0.8
        },
        text: {
            fontSize: 15, fontWeight: 'bold'
        },
        bar: {
            backgroundColor: 'white', width: '100%', height: 2, marginVertical: 5
        },
        img: {
            borderColor: '#73C6B6', borderWidth: 10, width: '100%', height: 200, borderRadius: 100, paddingTop: 5,
            backgroundColor: "#000000",
            opacity: 0.75,
            alignItems: "center"
        },
        overlay: {
            paddingTop: 5,
            backgroundColor: "#000000",
            opacity: 0.5,
            flexDirection: "column",
            alignItems: "center",
        },
        vaccinationWrap: {
            opacity: 0.8, marginTop: 12, marginHorizontal: 12, backgroundColor: '#73C6B6', borderRadius: 30, padding: 10
        },
        vaccinationText: {
            textAlign: "center", fontSize: 16, fontWeight: 'bold'
        }
    })

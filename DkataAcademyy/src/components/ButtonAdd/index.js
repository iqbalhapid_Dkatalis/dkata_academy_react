import React, {Component} from 'react'
import {Image, View, TouchableOpacity} from 'react-native'

export default class BtnAdd extends Component {
    render(){
        return(
            <TouchableOpacity style={{position:'absolute'}}>
                <Image source={require('../../public/img/add.png')} style={{width:50, height: 50, borderRadius: 25, marginLeft: 280, marginVertical: 10}}/>
            </TouchableOpacity>
        )
    }
}
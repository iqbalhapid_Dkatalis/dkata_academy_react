import React, { Component } from 'react'
import Container from './routes'
import { Provider } from 'react-redux'
import store from './store'


class App extends Component{
    render() {
        return (
        <Provider store = {store}>
            <Container />
        </Provider>
        )
    }
}

export default App
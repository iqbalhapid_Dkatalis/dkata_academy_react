import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Pets from '../components/Pets';
import profilePets from '../components/Pets/profilePets'

const RootStack = createStackNavigator(
    {

        Home: { screen: Pets },
        Profile: { screen: profilePets },
    },

    {

        initialRouteName: 'Home',
    }

);

const Container = createAppContainer(RootStack);

export default Container;
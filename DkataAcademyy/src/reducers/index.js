import {combineReducers} from 'redux'
import  getPetReducer  from './getPetReducer'

const appReducer = combineReducers({
    pets: getPetReducer
})

const rootReducer = (state, action) => {
    return appReducer(state, action)
}

export default rootReducer

